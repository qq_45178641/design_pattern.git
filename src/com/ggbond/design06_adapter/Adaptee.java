package com.ggbond.design06_adapter;

/**
 * @author ggbond
 * @date 2024年04月04日 10:25
 * 第三方接口，后面需 通过  Adapter 适配，使得Target兼容
 */
public class Adaptee {
    public void method1() {
        System.out.println("Adaptee：方法1");
    }

    public void method2() {
        System.out.println("Adaptee：方法2");
    }
}
