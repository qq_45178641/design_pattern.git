package com.ggbond.design13_ChainofRes;

/**
 * @author ggbond
 * @date 2024年04月10日 07:59
 */
public class Child extends  Handler{
    public Child(String name) {
        super(Handler.LEVEL1);
        this.name=name;
    }


    @Override
    protected void response(Question question) {
        System.out.print("我是"+name+" ");
        System.out.println("问题难度为1："+question.getDetail()+"已经被解决");
    }
}
