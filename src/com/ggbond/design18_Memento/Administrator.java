package com.ggbond.design18_Memento;

import java.io.PipedReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ggbond
 * @date 2024年04月16日 08:52
 */
public class Administrator {
    private int cursor=0;//游标 版本指针
    private Map<Integer, Memento> map=new HashMap<>();

    private List<Memento> list=new ArrayList<>();

    public void add(Memento memento){
        list.add(memento);
        map.put(memento.getLogFile().getVersion(),memento);
        cursor++;
    }
    public  Memento undo(){
        cursor--;
        if(cursor<=0){
            return list.get(0);
        }
        return list.get(cursor);
    }
    public  Memento redo(){
        cursor++;
        if(cursor>list.size()){
            return list.get(list.size()-1);
        }
        return list.get(cursor);
    }
    public  Memento get(int version){
        return  map.get(version);
    }
}
