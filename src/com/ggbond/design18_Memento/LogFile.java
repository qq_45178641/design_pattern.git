package com.ggbond.design18_Memento;

import java.util.Date;

/**
 * @author ggbond
 * @date 2024年04月16日 08:38
 */
public class LogFile {
    private  String name;
    private  int size;
    private Date time;
    private int version;

    public LogFile(String name, int size, Date time, int version) {
        this.name = name;
        this.size = size;
        this.time = time;
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "LogFile{" +
                "name='" + name + '\'' +
                ", size=" + size +
                ", time=" + time +
                ", version=" + version +
                '}';
    }
}
