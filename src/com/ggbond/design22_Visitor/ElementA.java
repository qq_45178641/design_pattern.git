package com.ggbond.design22_Visitor;

/**
 * @author ggbond
 * @date 2024年04月19日 08:46
 */
public class ElementA implements Element {

    // 文件夹名
    private String name;
    private String permissions;

    public ElementA(String name, String permissions) {
        this.name = name;
        this.permissions = permissions;
    }

    public String getName() {
        return name;
    }

    public String getPermissions() {
        return permissions;
    }
    @Override
    public void accept(Visitor v) {
        v.visit(this);
    }
}
