package com.ggbond.design22_Visitor;

public interface Visitor {
    void visit(ElementA a);
    void visit(ElementB a);
}
