package com.ggbond.design22_Visitor;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ggbond
 * @date 2024年04月19日 08:48
 */
public class Root implements Visitor {

    private final List<String> permissions=new ArrayList<>(List.of("read","write"));

    @Override
    public void visit(ElementA a) {
        if (permissions.contains(a.getPermissions())){
            System.out.println("该用户有权利访问"+a.getName());
        }else {
            System.out.println("该用户无权利访问"+a.getName());
        }
    }

    @Override
    public void visit(ElementB a) {
        if (permissions.contains(a.getPermissions())){
            System.out.println("该用户有权利访问"+a.getName());
        }else {
            System.out.println("该用户无权利访问"+a.getName());
        }
    }
}
