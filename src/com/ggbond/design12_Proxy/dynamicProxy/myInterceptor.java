package com.ggbond.design12_Proxy.dynamicProxy;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @author ggbond
 * @date 2024年04月09日 15:46
 * cglib 代理 ，需导入  cglib.jar, ASM.jar包
 */
public class myInterceptor  implements MethodInterceptor {
    private   Object target;

    public myInterceptor(Object target) {
        this.target = target;
    }
    public  void  before(){
        System.out.println("cglib代理人员即将进行配送");
    }

    public  void  after(){
        System.out.println("cglib代理人完成配送");
    }
    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        Object object;
        if ("toSend".equals(method.getName())){ //反射拦截注入
            before();
            object = method.invoke(target, objects);
            after();
        }else {
            object = method.invoke(target, objects);
        }
        return  object;
    }
}
