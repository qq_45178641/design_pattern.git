package com.ggbond.design12_Proxy.dynamicProxy;



/**
 * @author ggbond
 * @date 2024年04月09日 15:23
 */
public class Send implements Isend {
    @Override
    public void toSend() {
        System.out.println("送快递中");
    }
}
