package com.ggbond.design12_Proxy.dynamicProxy;

/**
 * @author ggbond
 * @date 2024年04月09日 15:22
 */
public interface Isend {
    void toSend();
}
