package com.ggbond.design19_State;

//在某种状态下，能做什么事情。
public interface State {
    void play();
    void  study();
    void  battle();
}
