package com.ggbond.design19_State;

/**
 * @author ggbond
 * @date 2024年04月17日 08:43
 */
public class Main {
    public static void main(String[] args) {
        Context context = new Context(); // 新建一个上下文
        context.setState(new normalState()); // 设置当前状态
        context.play();
        context.study();
        context.battle();// 调用行为
        System.out.println("===============");

        context.setState(new lollipopsState());
        context.play();
        context.study();
        context.battle();// 调用行为
    }
}
