package com.ggbond.design19_State;

/**
 * @author ggbond
 * @date 2024年04月17日 08:34
 * 猪猪侠 正常状态
 */
public class normalState implements  State{
    @Override
    public void play() {
        System.out.println("在望子成龙小学，与同学玩耍");
    }

    @Override
    public void study() {
        System.out.println("在望子成龙小学，认真学习");
    }

    @Override
    public void battle() {
        System.out.println("无战斗技能");
    }
}
