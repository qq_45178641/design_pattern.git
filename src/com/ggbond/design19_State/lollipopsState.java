package com.ggbond.design19_State;

/**
 * @author ggbond
 * @date 2024年04月17日 08:35
 * 猪猪侠 吃超级棒棒糖后的状态
 */
public class lollipopsState implements State{
    @Override
    public void play() {
        System.out.println("不玩耍");
    }

    @Override
    public void study() {
        System.out.println("学习效率提升300%");
    }

    @Override
    public void battle() {
        System.out.println("释放：降猪十八掌");
    }
}
