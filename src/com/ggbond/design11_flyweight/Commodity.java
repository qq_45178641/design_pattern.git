package com.ggbond.design11_flyweight;

/**
 * @author ggbond
 * @date 2024年04月09日 08:17
 *
 */
public class Commodity {
    private  long cid;
    private  String name;
    private  String type;
    private  String typeId;

    private String desc;
    //库存属性 是动态的，拆分成一个类 stockManage
    private  stockManage stockManage;

    public Commodity(long cid, String name, String type, String typeId, String desc) {
        this.cid = cid;
        this.name = name;
        this.type = type;
        this.typeId = typeId;
        this.desc = desc;
    }

    public long getCid() {
        return cid;
    }

    public void setCid(long cid) {
        this.cid = cid;
    }

    public stockManage getStockManage() {
        return stockManage;
    }

    public void setStockManage(stockManage stockManage) {
        this.stockManage = stockManage;
    }



    public long getId() {
        return cid;
    }

    public void setId(long id) {
        this.cid = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "Commodity{" +
                "cid=" + cid +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", typeId='" + typeId + '\'' +
                ", desc='" + desc + '\'' +
                ", stockManage=" + stockManage +
                '}';
    }
}
