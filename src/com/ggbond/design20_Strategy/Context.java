package com.ggbond.design20_Strategy;

/**
 * @author ggbond
 * @date 2024年04月18日 08:11
 */
public class Context {
    private Istrategy strategy;

    public void setStrategy(Istrategy strategy) {
        this.strategy = strategy;
    }

    public void discount(){
        strategy.discount();
    }
}
