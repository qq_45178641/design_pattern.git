package com.ggbond.design20_Strategy;

/**
 * @author ggbond
 * @date 2024年04月18日 08:02
 */
public interface Istrategy {
    void discount();
}
