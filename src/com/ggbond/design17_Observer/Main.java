package com.ggbond.design17_Observer;

/**
 * @author ggbond
 * @date 2024年04月15日 10:14
 */
public class Main {
    public static void main(String[] args) {

        //发布者
        Publisher publisher=new Publisher();

        //订阅者
        Subscribe s1=new Subscriber("ggbond1");
        Subscribe s2=new Subscriber("ggbond2");
        Subscribe s3=new Subscriber("ggbond3");

        //订阅者订阅主题
        publisher.attach(s1);
        publisher.attach(s2);
        publisher.attach(s3);

        //通知
        publisher.notifySubscriber();

    }
}
