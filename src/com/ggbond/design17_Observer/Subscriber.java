package com.ggbond.design17_Observer;

/**
 * @author ggbond
 * @date 2024年04月15日 10:11
 */
public class Subscriber implements Subscribe{
    private String name;

    public Subscriber(String name) {

        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public void getinfo() {
        System.out.println(name+":收到最新信息");
    }
}
