package com.ggbond.design08_component;

import java.awt.*;

/**
 * @author ggbond
 * @date 2024年04月06日 08:57
 * 叶子节点 三级部门
 */
public class Leaf extends Compound {
    public Leaf(String name) {
        super(name);
    }

    @Override
    public void add(Compound component) {
        System.out.println("不能加子部门");
    }

    @Override
    public void remove(Compound component) {
        System.out.println("无子部门，无删除权限");
    }

    @Override
    public void select(int depth) {
        //输出树形结构的叶子节点，这里直接输出设备名称
        for(int i = 0;  i < depth; i++) {
            System.out.print("*");
        }
        System.out.print(" ");
        System.out.println(getName());
    }
}
