package com.ggbond.design08_component;

/**
 * @author ggbond
 * @date 2024年04月06日 09:04
 */
public class Main {
    public static void main(String[] args) {
        Composite root=new Composite("一级部门1");
        //二级部门1
        Composite r21=new Composite("二级部门1");
        r21.add(new Composite("三级部门1"));
        r21.add(new Composite("三级部门2"));
        r21.add(new Composite("三级部门3"));
        //二级部门2
        Composite r22=new Composite("二级部门2");
        r22.add(new Composite("三级部门4"));
        r22.add(new Composite("三级部门5"));
        r22.add(new Composite("三级部门6"));
        //二级部门3
        Composite r23=new Composite("二级部门3");

        root.add(r21); root.add(r22); root.add(r23);
        root.select(1);
    }
}
