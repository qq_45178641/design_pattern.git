package com.ggbond.design08_component;

import java.awt.*;
import java.util.ArrayList;

/**
 * @author ggbond
 * @date 2024年04月06日 09:00
 */
public class Composite extends  Leaf{
    // 记录子部门信息
    private ArrayList<Compound> list = new ArrayList<>();

    public Composite(String name) {
        super(name);
    }

    @Override
    public void add(Compound component) {
        list.add(component);
    }

    @Override
    public void remove(Compound component) {
        list.remove(component);
    }

    @Override
    public void select(int depth) {
        for (int i = 0; i < depth-1; i++) {
            System.out.print(" ");
        }
        for (int i = 0; i < depth; i++) {
            System.out.print("*");
        }
        System.out.print(" ");
        System.out.println(getName());
        // 递归
        for(Compound compound: list) {
            compound.select(depth + 1);
        }
    }
}
