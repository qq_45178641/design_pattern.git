package com.ggbond.design09_decorator;

/**
 * @author ggbond
 * @date 2024年04月07日 10:11
 * 耳机 （实体）需要装饰的对象
 */
public class HeadPhoneSony  extends Iheadphone{

    private  String name;

    public HeadPhoneSony(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return  name;
    }

    @Override
    public void play() {
        System.out.println(this.getName()+"：开机");
    }
}
