package com.ggbond.design09_decorator;

/**
 * @author ggbond
 * @date 2024年04月07日 10:06
 * 耳机 （抽象）
 */
public abstract class Iheadphone {


    public abstract String getName();

    public abstract void play(); // 获取要播放的媒体文件名称

}
