package com.ggbond.design02_abstractFactory;

import com.ggbond.design02_abstractFactory.impl.IronBed;
import com.ggbond.design02_abstractFactory.impl.OfficeChair;
import com.ggbond.design02_abstractFactory.impl.PaintedBoardBedsideTable;

/**
 * @author ggbond
 * @date 2024年04月01日 08:53
 * 工厂2 负责 生产  铁艺床， 油漆床头柜，办公椅
 */
public class Factory02 implements Ifactory{

    static {
        System.out.println("我是工厂02");
    }
    @Override
    public Bed productBed() {
        return new IronBed();
    }

    @Override
    public BedsideTable productBedsideTable() {
        return new PaintedBoardBedsideTable();
    }

    @Override
    public Chair productChair() {
        return new OfficeChair();
    }
}
