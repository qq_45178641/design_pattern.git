package com.ggbond.design02_abstractFactory.impl;


import com.ggbond.design02_abstractFactory.Bed;

/**
 * @author ggbond
 * @date 2024年03月31日 15:46
 */
public class IronBed implements Bed {
    @Override
    public void getInfo() {
        System.out.println("生产了一个 铁艺床");
    }


}
