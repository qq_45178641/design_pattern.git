package com.ggbond.design02_abstractFactory.impl;

import com.ggbond.design02_abstractFactory.BedsideTable;

/**
 * @author ggbond
 * @date 2024年04月01日 08:38
 */
public class SolidWoodBedsideTable implements BedsideTable {
    @Override
    public void getInfo_b() {
        System.out.println("生产了一个 实木床头柜");
    }
}
