package com.ggbond.design02_abstractFactory.impl;

import com.ggbond.design02_abstractFactory.Chair;

/**
 * @author ggbond
 * @date 2024年04月01日 08:42
 */
public class DiningChair implements Chair {
    @Override
    public void getInfo_c() {
        System.out.println("生产了一个 餐椅");
    }
}
