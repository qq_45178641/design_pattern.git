package com.ggbond.design02_abstractFactory;

import com.ggbond.design02_abstractFactory.impl.DiningChair;
import com.ggbond.design02_abstractFactory.impl.SolidWoodBedsideTable;
import com.ggbond.design02_abstractFactory.impl.WoodBed;

/**
 * @author ggbond
 * @date 2024年04月01日 08:53
 * 工厂1 负责 生产  实木床， 实木床头柜，餐厅椅
 */
public class Factory01 implements Ifactory{
    static {
        System.out.println("我是工厂01");
    }
    @Override
    public Bed productBed() {
        return new WoodBed();
    }

    @Override
    public BedsideTable productBedsideTable() {
        return new SolidWoodBedsideTable();
    }

    @Override
    public Chair productChair() {
        return new DiningChair();
    }
}
