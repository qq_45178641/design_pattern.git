package com.ggbond.design02_abstractFactory;

/**
 * @author ggbond
 * @date 2024年04月01日 08:48
 * 一个工厂 能同时生产 床，床头柜，椅子
 */
public interface Ifactory {
    Bed productBed();
    BedsideTable productBedsideTable();
    Chair productChair();

}
