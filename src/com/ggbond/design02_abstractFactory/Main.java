package com.ggbond.design02_abstractFactory;

import com.ggbond.design02_abstractFactory.impl.WoodBed;

/**
 * @author ggbond
 * @date 2024年04月01日 08:59
 */
public class Main {
    public static void main(String[] args) {
        Ifactory factory01=new Factory01();

        Bed bed=factory01.productBed();
        BedsideTable bedsideTable = factory01.productBedsideTable();
        Chair chair = factory01.productChair();

        bed.getInfo();
        bedsideTable.getInfo_b();
        chair.getInfo_c();

        System.out.println("---------");

        Ifactory factory02=new Factory02();

        Bed bed02=factory02.productBed();
        BedsideTable bedsideTable02 = factory02.productBedsideTable();
        Chair chair02 = factory02.productChair();

        bed02.getInfo();
        bedsideTable02.getInfo_b();
        chair02.getInfo_c();

    }
}
