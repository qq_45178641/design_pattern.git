package com.ggbond.design14_Command;

/**
 * @author ggbond
 * @date 2024年04月12日 15:27
 */
public class WatercolorPen implements Stationery {
    private Producer producer;

    public WatercolorPen(Producer producer) {
        this.producer = producer;
    }

    @Override
    public void make() {
        producer.doProduce();
    }
}
