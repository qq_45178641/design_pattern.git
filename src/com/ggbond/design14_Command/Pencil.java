package com.ggbond.design14_Command;

/**
 * @author ggbond
 * @date 2024年04月12日 15:26
 */
public class Pencil implements Stationery{
    private Producer producer;

    public Pencil(Producer producer) {
        this.producer = producer;
    }

    @Override
    public void make() {
        producer.doProduce();
    }
}
