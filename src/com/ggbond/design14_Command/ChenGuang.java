package com.ggbond.design14_Command;

/**
 * @author ggbond
 * @date 2024年04月12日 15:31
 */
public class ChenGuang implements Producer {
    @Override
    public void doProduce() {
        System.out.println("晨光品牌——————值得信赖");
    }
}
