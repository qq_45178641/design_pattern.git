package com.ggbond.design14_Command;

public interface Producer {
    void  doProduce();
}
