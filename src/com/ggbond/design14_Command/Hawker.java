package com.ggbond.design14_Command;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author ggbond
 * @date 2024年04月12日 15:36
 */
public class Hawker {
    private List<Stationery> stationeries=new ArrayList<>();
    public void stock(Stationery item){
        stationeries.add(item);
    }
    public  void all(){
        for (Stationery item: stationeries){
            item.make();
        }
    }
}
