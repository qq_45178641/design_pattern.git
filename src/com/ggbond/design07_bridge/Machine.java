package com.ggbond.design07_bridge;

/**
 * @author ggbond
 * @date 2024年04月05日 16:44
 *   实现层次 （例如 下面机器）
 */
public abstract class Machine {

   public abstract void start();
   public abstract void  shutdown();

   public abstract  void setVolume(int value);
}
