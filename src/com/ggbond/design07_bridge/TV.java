package com.ggbond.design07_bridge;

/**
 * @author ggbond
 * @date 2024年04月05日 16:54
 * 设备实体-电视
 */
public class TV  extends Machine{
    @Override
    public void start() {
        System.out.println("TV 开机");
    }

    @Override
    public void shutdown() {
        System.out.println("TV 关机");
    }

    @Override
    public void setVolume(int value) {
        System.out.println("TV 设置 声音大小");
    }
}
