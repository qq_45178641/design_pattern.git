package com.ggbond.design07_bridge;

/**
 * @author ggbond
 * @date 2024年04月05日 16:57
 */
public class AirController extends  Controller{
    public AirController(Machine machine) {
        super(machine);
    }

    @Override
    public void cStart() {
        this.machine.start();
    }

    @Override
    public void vUp() {
        this.machine.setVolume(10);
    }

    @Override
    public void vdown() {
        this.machine.setVolume(0);
    }
    //自己可扩展设备接口
    public  void start(){
        System.out.println("通过遥控器开启空调");
        cStart();
    }
}
