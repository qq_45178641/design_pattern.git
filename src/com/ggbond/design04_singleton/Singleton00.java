package com.ggbond.design04_singleton;

/**
 * @author ggbond
 * @date 2024年04月03日 07:56
 * 饿汉式
 */
public class Singleton00 {
    private  static Singleton00 instance;
    //构造私有，外部不能通过new,获取实例
    private Singleton00(){};
    //线程不安全
    public static Singleton00 getInstance(){
        if(null==instance) {
            instance=new Singleton00();
        }
        return  instance;
    }
    //线程安全
    public static  synchronized Singleton00 getInstance1(){
        if(null==instance) {
            instance=new Singleton00();
        }
        return  instance;
    }
}
