package com.ggbond.design04_singleton;

/**
 * @author ggbond
 * @date 2024年04月03日 08:24
 */
public enum Singleton04 {
    INSTANCE;
    public void  test(){
        System.out.println("枚举单例");
    }
}
