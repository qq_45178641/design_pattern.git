package com.ggbond.design04_singleton;

/**
 * @author ggbond
 * @date 2024年04月03日 08:25
 */
public class test_singleton04 {
    public static void main(String[] args) {
        Singleton04 s1=Singleton04.INSTANCE;
        Singleton04 s2=Singleton04.INSTANCE;
        System.out.println(s1==s2);//true
    }
}
