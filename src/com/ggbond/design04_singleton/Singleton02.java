package com.ggbond.design04_singleton;

/**
 * @author ggbond
 * @date 2024年04月03日 07:49
 * DCL 线程安全
 */
public class Singleton02 {
    private volatile   static Singleton02 instance;

    private Singleton02(){};

    public Singleton02 getInstance(){
        if (instance!=null){  //防止 已有对象，还在加锁后判断。目的减少性能损耗
            return  instance;
        }else {
            synchronized(Singleton02.class) {//A与B同时到这里，A拿锁进去，B在这里等待。
                if (instance==null){ // 这里判断的原因，是A new完对象后，防止B进来又new一次。
                    instance=new Singleton02();
                }
            }
            return  instance;
        }
    }
}
