package com.ggbond.design05_prototype.deepClone;

import com.ggbond.design05_prototype.manKind01.Mankind01;

import java.util.Date;

/**
 * @author ggbond
 * @date 2024年04月03日 08:38
 */
public class Mankind02 implements Cloneable {
    private  int age;
    private Date birth;

    public Mankind02(int age, Date birth){
        this.age=age;
        this.birth=birth;
    }

    public int getAge() {
        return age;
    }

    public Date getBirth() {
        return birth;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    @Override
    protected Mankind02 clone() throws CloneNotSupportedException {
        Mankind02 m = (Mankind02) super.clone();
        m.setBirth((Date) birth.clone());
        return m;
    }
}
