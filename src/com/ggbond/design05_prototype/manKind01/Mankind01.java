package com.ggbond.design05_prototype.manKind01;

import com.ggbond.design05_prototype.deepClone.Mankind02;

import java.util.Date;

/**
 * @author ggbond
 * @date 2024年04月03日 08:38
 */
public class Mankind01 implements Cloneable {
    private  int age;
    private Date birth;

    public Mankind01(int age, Date birth){
        this.age=age;
        this.birth=birth;
    }

    public int getAge() {
        return age;
    }

    public Date getBirth() {
        return birth;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    @Override
    protected Mankind01 clone() throws CloneNotSupportedException {

        return (Mankind01) super.clone();

    }
}
