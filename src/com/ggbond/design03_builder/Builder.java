package com.ggbond.design03_builder;

import com.ggbond.design03_builder.component.*;

/**
 * @author ggbond
 * @date 2024年04月02日 10:16
 */
public class Builder {
    public Ihouse type1(float area,String name){
        return new House(area,name)
                .addDoor(new WoodDoor())
                .addFloor(new WoodFloor())
                .addWindow(new SlidingWindow())
                .addRoof(new TileRoof());
    }

    public Ihouse type2(float area,String name){
        return new House(area,name)
                .addDoor(new GlassDoor())
                .addFloor(new MarbleFloor())
                .addWindow(new CasementWindow())
                .addRoof(new CementRoof());
    }
}
