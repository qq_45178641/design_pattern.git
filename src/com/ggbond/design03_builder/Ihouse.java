package com.ggbond.design03_builder;

/**
 * @author ggbond
 * @date 2024年04月02日 09:29
 * 房子组成要素
 */
public interface Ihouse {
    Ihouse addDoor(Icomponent door);
    Ihouse addFloor(Icomponent floor);
    Ihouse addWindow(Icomponent window);
    Ihouse addRoof(Icomponent roof);
    String getInfo();
}
