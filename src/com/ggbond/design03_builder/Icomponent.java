package com.ggbond.design03_builder;

import java.math.BigDecimal;

/**
 * @author ggbond
 * @date 2024年04月02日 09:28
 */
public interface Icomponent {
     String position(); //材料分类 ：地板 屋顶 门 窗
     String type(); //具体材料 如 木窗 ，水泥屋顶

     BigDecimal price(); //每平方米的价格

}
