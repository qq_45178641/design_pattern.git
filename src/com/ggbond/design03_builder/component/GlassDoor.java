package com.ggbond.design03_builder.component;

import com.ggbond.design03_builder.Icomponent;

import java.math.BigDecimal;

/**
 * @author ggbond
 * @date 2024年04月02日 09:35
 * 玻璃门
 */
public class GlassDoor implements Icomponent {

    @Override
    public String position() {
        return "门";
    }

    @Override
    public String type() {
        return "玻璃门";
    }

    @Override
    public BigDecimal price() {
        return new BigDecimal(130);
    }
}
