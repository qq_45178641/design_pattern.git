package com.ggbond.design03_builder.component;

import com.ggbond.design03_builder.Icomponent;

import java.math.BigDecimal;

/**
 * @author ggbond
 * @date 2024年04月02日 09:35
 * 木质地板
 */
public class WoodFloor implements Icomponent {
    @Override
    public String position() {
        return "地板";
    }

    @Override
    public String type() {
        return "木质地板";
    }

    @Override
    public BigDecimal price() {
        return new BigDecimal(67);
    }
}
