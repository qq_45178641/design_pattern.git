package com.ggbond.design03_builder.component;

import com.ggbond.design03_builder.Icomponent;

import java.math.BigDecimal;

/**
 * @author ggbond
 * @date 2024年04月02日 09:37
 * 推拉窗
 */
public class SlidingWindow implements Icomponent {
    @Override
    public String position() {
        return "窗户";
    }

    @Override
    public String type() {
        return "旋转窗";
    }

    @Override
    public BigDecimal price() {
        return new BigDecimal(80);
    }
}
