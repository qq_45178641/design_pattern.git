package com.ggbond.design03_builder;

/**
 * @author ggbond
 * @date 2024年04月02日 10:19
 */
public class Main {
    public static void main(String[] args) {
        Builder builder=new Builder();
        System.out.println(builder.type1(60f,"ggbond_house1").getInfo());
        System.out.println(builder.type2(120f,"ggbond_house2").getInfo());
    }
}
