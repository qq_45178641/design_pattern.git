package com.ggbond.design16_Mediator;

/**
 * @author ggbond
 * @date 2024年04月14日 16:56
 */
public class Person01  extends Person{
    private  Mediator mediator;
    public Person01(String name,Mediator mediator) {
        super(name);
        this.mediator=mediator;
    }

    @Override
    public void send() {
        System.out.println(this.getName()+"发布出租信息");
        this.mediator.info(this);//中介转发
    }

    @Override
    public void receive() {
        System.out.println(this.getName()+"收到其他人发布的出租信息");
    }
}
