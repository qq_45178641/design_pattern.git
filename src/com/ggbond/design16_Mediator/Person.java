package com.ggbond.design16_Mediator;

/**
 * @author ggbond
 * @date 2024年04月14日 16:55
 */
public abstract class Person {
    private String name;

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public abstract  void  send();
    public abstract  void  receive();
}
