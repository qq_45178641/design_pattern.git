package com.ggbond.design16_Mediator;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ggbond
 * @date 2024年04月14日 17:00
 */
public class HouseMediator  extends  Mediator{
    private List<Person> list=new ArrayList<>();

    @Override
    void add(Person p1) {
        if (!list.contains(p1)){
            list.add(p1);
        }
    }

    @Override
    public void info(Person p1) {
        for ( Person item :list){
            if (!p1.equals(item)){
                item.receive();
            }
        }
    }
}
