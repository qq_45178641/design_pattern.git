package com.ggbond.design10_facade;

/**
 * @author ggbond
 * @date 2024年04月08日 10:40
 */
public class Main {
    public static void main(String[] args) {
        //客户端进行抽奖
        Lottery lottery=new Lottery();
        Delivery delivery=new Delivery();
       String id=lottery.getId();
        boolean stockControl = lottery.stockControl(id);
        if (stockControl) {
            delivery.createSaleId(id);//生成订单信息
            delivery.send(id); //发货
        }

        //门面模式 抽奖封装,客户端执行封装好的一个服务方法。
        Facade facade=new Facade(lottery,delivery);
        facade.toLottery();
    }
}
