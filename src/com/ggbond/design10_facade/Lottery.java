package com.ggbond.design10_facade;

/**
 * @author ggbond
 * @date 2024年04月08日 10:34
 */
public class Lottery {
    public String  getId(){
        System.out.println("获取商品id");
        return  "00001";
    }
    public boolean  stockControl(String id){
        System.out.println("库存减一");
        return true;
    }
}
