package com.ggbond.design10_facade;

/**
 * @author ggbond
 * @date 2024年04月08日 10:38
 */
public class Delivery {
    public String  createSaleId(String id){
        System.out.println("商品打包，生产快递订单号");
        return  "00001";
    }
    public boolean  send(String id){
        System.out.println("发货");
        return true;
    }
}
