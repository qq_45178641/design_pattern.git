package com.ggbond.design01CreationalPattern;

/**
 * @author ggbond
 * @date 2024年03月31日 15:50
 */
public class BedFactory {
    private static  final  String CLASSPATH="com.ggbond.design01CreationalPattern.impl.";
    public Bed product(String bedType) throws Exception {
        if (bedType.isBlank()){
            return  null;
        }
        Class<?> clazz = Class.forName(CLASSPATH + bedType);
        return  (Bed) clazz.getDeclaredConstructor().newInstance();
    }
}
