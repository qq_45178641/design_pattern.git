package com.ggbond.design01CreationalPattern;

/**
 * @author ggbond
 * @date 2024年03月31日 15:42
 */
public interface Bed {
     void  getInfo();
}
