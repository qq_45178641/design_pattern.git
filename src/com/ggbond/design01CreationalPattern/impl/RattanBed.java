package com.ggbond.design01CreationalPattern.impl;

import com.ggbond.design01CreationalPattern.Bed;

/**
 * @author ggbond
 * @date 2024年03月31日 15:44
 */
public class RattanBed implements Bed {
    @Override
    public void getInfo() {

        System.out.println("生产了一个藤艺床");
    }
}
