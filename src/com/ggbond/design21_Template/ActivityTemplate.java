package com.ggbond.design21_Template;

/**
 * @author ggbond
 * @date 2024年04月18日 17:32
 * 发送奖品
 */
public abstract class ActivityTemplate {
    //模版流程
    public void reward (String info) {
        //模版流程 ，
        boolean check = check(info);//1.校验信息
        if (check) {
            chooseType(info); //1. 发送奖品
        }


    }
    public boolean check(String info){
        System.out.println("校验"+info+"身份信息");
        return true;
    }
    // 发送方式
    protected  abstract   void  chooseType(String info);
}
