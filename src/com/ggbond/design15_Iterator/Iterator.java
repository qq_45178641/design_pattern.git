package com.ggbond.design15_Iterator;
/**
 * 迭代抽象
 * */
public interface Iterator<A> {
    A next();
    boolean hasNext();
}
