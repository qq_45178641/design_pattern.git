package com.ggbond.design15_Iterator;

/**
 * @author ggbond
 * @date 2024年04月13日 09:08
 */
public class MyIterator<T> implements Iterator<T>{
    private MyCollection<T> myCollection;
    private  int size;
    private int index; //索引游标

    public MyIterator(MyCollection<T> myCollection) {
        this.myCollection = myCollection;
        this.index = 0;
        this.size = myCollection.getSize();
    }

    @Override
    public T next() {
        if (index < size) {
            return myCollection.getNext(index++);
        }
        return null;

    }

    @Override
    public boolean hasNext() {
        return index < size;
    }
}
