package com.ggbond.design15_Iterator;

/**
 * @author ggbond
 * @date 2024年04月13日 09:29
 */
public class Person {
    String name;
    String cardID;

    public Person(String name, String cardID) {
        this.name = name;
        this.cardID = cardID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCardID() {
        return cardID;
    }

    public void setCardID(String cardID) {
        this.cardID = cardID;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", cardID='" + cardID + '\'' +
                '}';
    }
}
