package com.ggbond.design15_Iterator;

import java.util.ArrayList;

/**
 * @author ggbond
 * @date 2024年04月13日 09:12
 */
public class MyCollection<T> implements Collection<T>{
    private ArrayList<T> list=new ArrayList<T>();
    public int getSize() {
        return list.size();
    }
    @Override
    public boolean add(T t) {
       return list.add(t);
    }

    @Override
    public boolean remove(T t) {
       return  list.remove(t);
    }

    @Override
    public Iterator createIterator() {
        return new MyIterator(this);
    }

    public T getNext(int i) {
        return  list.get(i);
    }
}
